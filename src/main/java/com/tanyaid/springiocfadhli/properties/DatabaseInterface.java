package com.tanyaid.springiocfadhli.properties;

public interface DatabaseInterface
{
    String mysql_host = "127.0.0.1";
    String mysql_port = "3306";
    String mysql_user = "root";
    String mysql_password = "";
    String mysql_database = "db_test_bankmandiri";
    String mysql_driver = "jdbc:mysql://";
    String mysql_url = mysql_driver + mysql_host + ":" + mysql_port + "/" + mysql_database + "?autoReconnect=true&verifyServerCertificate=false&useSSL=false&useLegacyDatetimeCode=false&serverTimezone=Asia/Jakarta";
}
