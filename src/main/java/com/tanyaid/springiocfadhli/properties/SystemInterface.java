package com.tanyaid.springiocfadhli.properties;

public interface SystemInterface
{
    String Message_Error_0001 = "Oops! Lengkapi data dengan benar!";
    String Message_Error_0003 = "Oops! Access denied.";
    String Message_Error_0004 = Message_Error_0003 + " Check your JSON raw format.";
    String Message_Error_0005 = "Oops! Data not found.";
    String Message_Error_0006 = "Oops! Failed save data.";
    String Message_Error_0007 = "Oops! Data duplicated.";
    String Message_Error_0008 = "Oops! Data was exists.";
    String Message_Error_0009 = "Oops! Failed delete data.";
    String Message_Error_0010 = "Oops! Failed update data.";

    String Message_Success_0001 = "Berikut ini datanya.";
    String Message_Success_0002 = "Save success.";
    String Message_Success_0003 = "Delete sucess.";
    String Message_Success_0004 = "Update success.";
}
