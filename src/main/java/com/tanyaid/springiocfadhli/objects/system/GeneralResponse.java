package com.tanyaid.springiocfadhli.objects.system;

import java.io.Serializable;
import java.util.Objects;
public class GeneralResponse implements Serializable
{
    private static final long serialVersionUID = 1770816922820314739L;

    private Boolean status;
    private String httpCode;
    private String message;
    private Object data;

    public static long getSerialVersionUID()
    {
        return serialVersionUID;
    }

    public Boolean getStatus()
    {
        return status;
    }

    public void setStatus(Boolean status)
    {
        this.status = status;
    }

    public String getHttpCode()
    {
        return httpCode;
    }

    public void setHttpCode(String httpCode)
    {
        this.httpCode = httpCode;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public Object getData()
    {
        return data;
    }

    public void setData(Object data)
    {
        this.data = data;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GeneralResponse that = (GeneralResponse) o;
        return status.equals(that.status) &&
                httpCode.equals(that.httpCode) &&
                message.equals(that.message) &&
                data.equals(that.data);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(status, httpCode, message, data);
    }

    @Override
    public String toString()
    {
        return "GeneralResponse{" +
                "status=" + status +
                ", httpCode='" + httpCode + '\'' +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
