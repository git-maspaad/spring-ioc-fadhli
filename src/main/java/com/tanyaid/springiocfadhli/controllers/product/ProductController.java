package com.tanyaid.springiocfadhli.controllers.product;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;
import com.tanyaid.springiocfadhli.dao.product.ProductDao;
import com.tanyaid.springiocfadhli.models.product.ProductModel;
import com.tanyaid.springiocfadhli.models.system.SystemLogModel;
import com.tanyaid.springiocfadhli.objects.system.GeneralResponse;
import com.tanyaid.springiocfadhli.properties.SystemInterface;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Produces;
import java.io.StringReader;

@RestController
@RequestMapping("/produk")
public class ProductController
{
    private static GeneralResponse generalResponse;
    private static JsonArray data;
    private static JsonObject rawObject;
    private static JsonReader jsonReader;

    @RequestMapping("")
    public String indexNull() { return SystemLogModel.index(); }

    @RequestMapping("/")
    public String index() { return SystemLogModel.index(); }

    @RequestMapping(value = "/add-new", method = RequestMethod.POST)
    @Produces(MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody String addNew(@RequestBody String requestParam)
    {
        generalResponse = new GeneralResponse();
        data = new JsonArray();

        try {
            if (StringUtils.isEmpty(requestParam))
            {
                throw new Exception(SystemInterface.Message_Error_0004);
            }

            jsonReader = new JsonReader(new StringReader(requestParam));
            jsonReader.setLenient(true);

            rawObject = new Gson().fromJson(jsonReader, JsonObject.class);

            if (rawObject.get("namaProduk") == null || rawObject.get("harga") == null)
            {
                throw new Exception(SystemInterface.Message_Error_0004);
            }
            else if (StringUtils.isEmpty(rawObject.get("namaProduk").getAsString()) || StringUtils.isEmpty(rawObject.get("harga").getAsString()))
            {
                throw new Exception(SystemInterface.Message_Error_0001);
            }
            String namaProduk = rawObject.get("namaProduk").getAsString();
            Integer harga = rawObject.get("harga").getAsInt();

            ProductModel productModel = new ProductModel();
            productModel.setNamaProduk(namaProduk);
            productModel.setHarga(harga);

            ProductDao productDao = new ProductDao();
            JsonObject checkProduct = productDao.checkProductName(productModel);
            if (checkProduct.get("status").getAsBoolean() == true)
            {
                throw new Exception(SystemInterface.Message_Error_0008);
            }

            Boolean isAddNew = productDao.create(productModel);
            if (isAddNew == false)
            {
                throw new Exception(SystemInterface.Message_Error_0006);
            }

            generalResponse.setStatus(isAddNew);
            generalResponse.setMessage(SystemInterface.Message_Success_0002);
        } catch (Exception ex) {
            generalResponse.setStatus(false);
            generalResponse.setMessage(ex.getMessage());
        }

        return new Gson().toJson(generalResponse);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    @Produces(MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody String delete(@RequestBody String requestParam)
    {
        generalResponse = new GeneralResponse();
        data = new JsonArray();

        try {
            if (StringUtils.isEmpty(requestParam))
            {
                throw new Exception(SystemInterface.Message_Error_0004);
            }

            jsonReader = new JsonReader(new StringReader(requestParam));
            jsonReader.setLenient(true);

            rawObject = new Gson().fromJson(jsonReader, JsonObject.class);

            if (rawObject.get("id") == null)
            {
                throw new Exception(SystemInterface.Message_Error_0004);
            }
            else if (StringUtils.isEmpty(rawObject.get("id").getAsString()))
            {
                throw new Exception(SystemInterface.Message_Error_0001);
            }
            Integer id = rawObject.get("id").getAsInt();

            ProductModel productModel = new ProductModel();
            productModel.setId(id);

            ProductDao productDao = new ProductDao();
            JsonObject checkProduct = productDao.getById(productModel);
            if (checkProduct.get("status").getAsBoolean() == false)
            {
                throw new Exception(checkProduct.get("message").getAsString());
            }

            Boolean isDelete = productDao.delete(productModel);
            if (isDelete == false)
            {
                throw new Exception(SystemInterface.Message_Error_0009);
            }

            generalResponse.setStatus(isDelete);
            generalResponse.setMessage(SystemInterface.Message_Success_0003);
        } catch (Exception ex) {
            generalResponse.setStatus(false);
            generalResponse.setMessage(ex.getMessage());
        }

        return new Gson().toJson(generalResponse);
    }

    @RequestMapping(value = "/details", method = RequestMethod.POST)
    @Produces(MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody String details(@RequestBody String requestParam)
    {
        generalResponse = new GeneralResponse();
        data = new JsonArray();

        try {
            if (StringUtils.isEmpty(requestParam))
            {
                throw new Exception(SystemInterface.Message_Error_0004);
            }

            jsonReader = new JsonReader(new StringReader(requestParam));
            jsonReader.setLenient(true);

            rawObject = new Gson().fromJson(jsonReader, JsonObject.class);

            if (rawObject.get("id") == null)
            {
                throw new Exception(SystemInterface.Message_Error_0004);
            }
            else if (StringUtils.isEmpty(rawObject.get("id").getAsString()))
            {
                throw new Exception(SystemInterface.Message_Error_0001);
            }
            Integer id = rawObject.get("id").getAsInt();

            ProductModel productModel = new ProductModel();
            productModel.setId(id);

            ProductDao productDao = new ProductDao();
            JsonObject checkProduct = productDao.getById(productModel);
            if (checkProduct.get("status").getAsBoolean() == false)
            {
                throw new Exception(checkProduct.get("message").getAsString());
            }

            generalResponse.setStatus(checkProduct.get("status").getAsBoolean());
            generalResponse.setMessage(checkProduct.get("message").getAsString());
            generalResponse.setData(checkProduct.get("data").getAsJsonArray());
        } catch (Exception ex) {
            generalResponse.setStatus(false);
            generalResponse.setMessage(ex.getMessage());
        }

        return new Gson().toJson(generalResponse);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @Produces(MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody String list()
    {
        generalResponse = new GeneralResponse();
        data = new JsonArray();

        try {
            ProductDao productDao = new ProductDao();
            JsonObject listProduct = productDao.getAll();
            if (listProduct.get("status").getAsBoolean() == false)
            {
                throw new Exception(listProduct.get("message").getAsString());
            }

            generalResponse.setStatus(listProduct.get("status").getAsBoolean());
            generalResponse.setMessage(listProduct.get("message").getAsString());
            generalResponse.setData(listProduct.get("data").getAsJsonArray());
        } catch (Exception ex) {
            generalResponse.setStatus(false);
            generalResponse.setMessage(ex.getMessage());
        }

        return new Gson().toJson(generalResponse);
    }

    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    @Produces(MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody String update(@RequestBody String requestParam)
    {
        generalResponse = new GeneralResponse();
        data = new JsonArray();

        try {
            if (StringUtils.isEmpty(requestParam))
            {
                throw new Exception(SystemInterface.Message_Error_0004);
            }

            jsonReader = new JsonReader(new StringReader(requestParam));
            jsonReader.setLenient(true);

            rawObject = new Gson().fromJson(jsonReader, JsonObject.class);

            if (rawObject.get("id") == null || rawObject.get("namaProduk") == null || rawObject.get("harga") == null)
            {
                throw new Exception(SystemInterface.Message_Error_0004);
            }
            else if (StringUtils.isEmpty(rawObject.get("id").getAsString()) || StringUtils.isEmpty(rawObject.get("namaProduk").getAsString()) || StringUtils.isEmpty(rawObject.get("harga").getAsString()))
            {
                throw new Exception(SystemInterface.Message_Error_0001);
            }
            Integer id = rawObject.get("id").getAsInt();
            String namaProduk = rawObject.get("namaProduk").getAsString();
            Integer harga = rawObject.get("harga").getAsInt();

            ProductModel productModel = new ProductModel();
            productModel.setId(id);
            productModel.setNamaProduk(namaProduk);
            productModel.setHarga(harga);

            ProductDao productDao = new ProductDao();
            JsonObject checkProduct = productDao.getById(productModel);
            if (checkProduct.get("status").getAsBoolean() == false)
            {
                throw new Exception(checkProduct.get("message").getAsString());
            }

            Boolean isUpdate = productDao.update(productModel);
            if (isUpdate == false)
            {
                throw new Exception(SystemInterface.Message_Error_0010);
            }

            generalResponse.setStatus(isUpdate);
            generalResponse.setMessage(SystemInterface.Message_Success_0004);
        } catch (Exception ex) {
            generalResponse.setStatus(false);
            generalResponse.setMessage(ex.getMessage());
        }

        return new Gson().toJson(generalResponse);
    }
}
