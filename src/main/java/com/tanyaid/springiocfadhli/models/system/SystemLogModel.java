package com.tanyaid.springiocfadhli.models.system;

import com.tanyaid.springiocfadhli.objects.system.GeneralResponse;
import com.tanyaid.springiocfadhli.properties.SystemInterface;
import com.google.gson.Gson;

public class SystemLogModel
{
    public static String index()
    {
        GeneralResponse generalResponse = new GeneralResponse();
        generalResponse.setStatus(false);
        generalResponse.setMessage(SystemInterface.Message_Error_0003);
        return new Gson().toJson(generalResponse);
    }
}
