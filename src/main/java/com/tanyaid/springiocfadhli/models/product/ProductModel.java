package com.tanyaid.springiocfadhli.models.product;

public class ProductModel
{
    private Integer id;
    private String namaProduk;
    private Integer harga;

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getNamaProduk()
    {
        return namaProduk;
    }

    public void setNamaProduk(String namaProduk)
    {
        this.namaProduk = namaProduk;
    }

    public Integer getHarga()
    {
        return harga;
    }

    public void setHarga(Integer harga)
    {
        this.harga = harga;
    }
}
