package com.tanyaid.springiocfadhli.system;

import com.mysql.jdbc.Driver;
import com.tanyaid.springiocfadhli.properties.DatabaseInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import java.sql.*;

public class MySQLConnection
{
    @Autowired
    private static Environment env;

    private static Connection connection;

    private static PreparedStatement preparedStatement;

    private static Statement statement;

    public static Connection getConnection()
    {
        try {
            String url = DatabaseInterface.mysql_url;
            String user = DatabaseInterface.mysql_user;
            String password = DatabaseInterface.mysql_password;

            DriverManager.registerDriver(new Driver());

            connection = DriverManager.getConnection(url, user, password);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return connection;
    }

    public static PreparedStatement getPreparedStatement(String query)
    {
        try {
            preparedStatement = MySQLConnection.getConnection().prepareStatement(query);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return preparedStatement;
    }

    public static Statement getStatement()
    {
        try {
            statement = MySQLConnection.getConnection().createStatement();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return statement;
    }
}
