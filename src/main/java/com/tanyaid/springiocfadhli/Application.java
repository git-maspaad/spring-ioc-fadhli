package com.tanyaid.springiocfadhli;

import com.tanyaid.springiocfadhli.models.system.SystemLogModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Application
{
    @GetMapping("")
    public String indexGetNull(){ return SystemLogModel.index(); }

    @GetMapping("/")
    public String indexGet(){ return SystemLogModel.index(); }

    @RequestMapping("")
    public String indexPostNull() { return SystemLogModel.index(); }

    @RequestMapping("/")
    public String indexPost() { return SystemLogModel.index(); }

    public static void main(String[] args)
    {
        SpringApplication.run(Application.class, args);
    }
}
