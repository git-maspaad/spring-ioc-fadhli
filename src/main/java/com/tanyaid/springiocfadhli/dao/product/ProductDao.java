package com.tanyaid.springiocfadhli.dao.product;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tanyaid.springiocfadhli.models.product.ProductModel;
import com.tanyaid.springiocfadhli.properties.SystemInterface;
import com.tanyaid.springiocfadhli.system.MySQLConnection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ProductDao
{
    public JsonObject checkProductName(ProductModel product)
    {
        JsonObject item = new JsonObject();
        String query = "SELECT * FROM tbl_product WHERE LCASE(nama_produk) = ?;";
        try {
            PreparedStatement preparedStatement = MySQLConnection.getPreparedStatement(query);
            preparedStatement.setString(1, product.getNamaProduk().toLowerCase());
            preparedStatement.execute();

            ResultSet resultSet = preparedStatement.getResultSet();
            JsonArray data = new JsonArray();
            while (resultSet.next())
            {
                JsonObject dataItem = new JsonObject();
                dataItem.addProperty("id", resultSet.getInt("id"));
                dataItem.addProperty("namaProduk", resultSet.getString("nama_produk"));
                dataItem.addProperty("harga", resultSet.getInt("harga"));
                data.add(dataItem);
            }
            resultSet.close();
            preparedStatement.close();

            item.addProperty("status", (data.size() == 1 ? true : false));
            item.addProperty("message", (data.size() == 1 ? SystemInterface.Message_Success_0001 : (data.size() > 1 ? SystemInterface.Message_Error_0007 : SystemInterface.Message_Error_0005)));
            if (data.size() == 1) { item.add("data", data); }
            return item;
        } catch (SQLException ex) {
            item.addProperty("status", false);
            item.addProperty("message", SystemInterface.Message_Error_0005);
            return item;
        }
    }

    public Boolean create(ProductModel product)
    {
        String query = "INSERT INTO tbl_product(nama_produk, harga) VALUES(?, ?);";
        try {
            PreparedStatement preparedStatement = MySQLConnection.getPreparedStatement(query);
            preparedStatement.setString(1, product.getNamaProduk());
            preparedStatement.setInt(2, product.getHarga());
            preparedStatement.executeUpdate();
            preparedStatement.close();
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    public Boolean update(ProductModel product)
    {
        String query = "UPDATE tbl_product SET nama_produk = ?, harga = ? WHERE id = ?;";
        try {
            PreparedStatement preparedStatement = MySQLConnection.getPreparedStatement(query);
            preparedStatement.setString(1, product.getNamaProduk());
            preparedStatement.setInt(2, product.getHarga());
            preparedStatement.setInt(3, product.getId());
            preparedStatement.executeUpdate();
            preparedStatement.close();
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    public Boolean delete(ProductModel product)
    {
        String query = "DELETE FROM tbl_product WHERE id = ?;";
        try {
            PreparedStatement preparedStatement = MySQLConnection.getPreparedStatement(query);
            preparedStatement.setInt(1, product.getId());
            preparedStatement.executeUpdate();
            preparedStatement.close();
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    public JsonObject getById(ProductModel product)
    {
        JsonObject item = new JsonObject();
        String query = "SELECT * FROM tbl_product WHERE id = ?;";
        try {
            PreparedStatement preparedStatement = MySQLConnection.getPreparedStatement(query);
            preparedStatement.setInt(1, product.getId());
            preparedStatement.execute();

            ResultSet resultSet = preparedStatement.getResultSet();
            JsonArray data = new JsonArray();
            while (resultSet.next())
            {
                JsonObject dataItem = new JsonObject();
                dataItem.addProperty("id", resultSet.getInt("id"));
                dataItem.addProperty("namaProduk", resultSet.getString("nama_produk"));
                dataItem.addProperty("harga", resultSet.getInt("harga"));
                data.add(dataItem);
            }
            resultSet.close();
            preparedStatement.close();

            item.addProperty("status", (data.size() == 1 ? true : false));
            item.addProperty("message", (data.size() == 1 ? SystemInterface.Message_Success_0001 : (data.size() > 1 ? SystemInterface.Message_Error_0007 : SystemInterface.Message_Error_0005)));
            if (data.size() == 1) { item.add("data", data); }
            return item;
        } catch (SQLException ex) {
            item.addProperty("status", false);
            item.addProperty("message", SystemInterface.Message_Error_0005);
            return item;
        }
    }

    public JsonObject getAll()
    {
        JsonObject item = new JsonObject();
        String query = "SELECT * FROM tbl_product ORDER BY nama_produk ASC, harga DESC;";
        try {
            PreparedStatement preparedStatement = MySQLConnection.getPreparedStatement(query);
            preparedStatement.execute();

            ResultSet resultSet = preparedStatement.getResultSet();
            JsonArray data = new JsonArray();
            while (resultSet.next())
            {
                JsonObject dataItem = new JsonObject();
                dataItem.addProperty("id", resultSet.getInt("id"));
                dataItem.addProperty("namaProduk", resultSet.getString("nama_produk"));
                dataItem.addProperty("harga", resultSet.getInt("harga"));
                data.add(dataItem);
            }
            resultSet.close();
            preparedStatement.close();

            item.addProperty("status", (data.size() > 0 ? true : false));
            item.addProperty("message", (data.size() > 0 ? SystemInterface.Message_Success_0001 : SystemInterface.Message_Error_0005));
            if (data.size() > 0) { item.add("data", data); }
            return item;
        } catch (SQLException ex) {
            item.addProperty("status", false);
            item.addProperty("message", SystemInterface.Message_Error_0005);
            return item;
        }
    }
}
