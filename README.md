# Product API with SpringBoot
I choose Java for API Development, and Docker as my favourite tool for deployment.

<strong>Open API Documentation</strong>&nbsp;&nbsp;&nbsp;([click here](https://documenter.getpostman.com/view/6414152/UVyn1J7i)).

# Steps
1. Open Terminal
2. Clone this repository by type git clone https://gitlab.com/git-maspaad/spring-ioc-fadhli.git
3. Move path position cd spring-ioc-fadhli
4. Let's begin write code for API

# Maven Version
1. In 2022, I use maven version 3.8.4 ([click here to download](https://dlcdn.apache.org/maven/maven-3/3.8.4/binaries/apache-maven-3.8.4-bin.zip)).
2. Extract .zip file
3. Don't forget add to your system path (Mac or Linux) or environment variables (Windows).

# Development
1. If you want to clean the snapshot etc inside your project, type ./mvnw clean
2. If you want to compile it the pom.xml configuration, type ./mvnw compile
3. But, if you get some troubles while compiling, you can skip the test process by typing ./mvnw -Dmaven.test.skip=true compile
4. Pre-test install spring boot application, type ./mvnw install -U
4. If you want to run the project, type ./mvnw spring-boot:run
5. If you won't get error or skip any process, type ./mvnw -Dmaven.test.skip=true spring-boot:run
6. Finally, the project has to compile being .jar file by typing ./mvnw package
7. Still trouble or got some errors while compile it to be .jar file? Just skip anything wrong of the test process by typing ./mvnw -Dmaven.test.skip=true package
8. File .jar is inside directory named "target".
9. All complete commands at once, type ./mvnw clean && ./mvnw compile -Dmaven.test.skip=true && ./mvnw install -U && ./mvnw package -Dmaven.test.skip=true && ./mvnw -Dmaven.test.skip=true spring-boot:run

# Deployment & Test for API
1. Run .jar file with these command : java -jar ./spring-ioc-fadhli-0.0.1-SNAPSHOT.jar ./application.properties
2. See the log in your screen, which port the file run on to it. As default, it would have 8080 as default port.
3. Open application tools for API testing, I choose "Postman" as my tools.
4. Test your API worked well, type in url box -> http://127.0.0.1:5000

# Attention! for deployment of Docker
1. Check container is running, docker container ls -a
2. If exist one of service is running, stop it -> docker stop containerId
3. Then remove the container, docker rm containerId
4. Remove existing image, docker rmi imageId

# Deployment into Docker
1. Check images, docker images
2. Check container, docker container ls -a
3. Build new image, docker build -t img-spring-ioc-fadhli:1.0 .
4. Create container & run, docker run -it --name ctr-spring-ioc-fadhli -p 10000:5000 -d img-spring-ioc-fadhli:1.0
5. Save image into archive file, docker save -o ./docker-spring-ioc-fadhli.tar img-spring-ioc-fadhli:1.0
6. Load image from archive file, docker load < ./spring-ioc-fadhli.tar
7. Run docker, docker run -it --name ctr-spring-ioc-fadhli -p 10000:5000 -d img-spring-ioc-fadhli:1.0

# Java Spring template project
This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).
Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/spring).

# CI/CD with Auto DevOps
This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).
If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.
